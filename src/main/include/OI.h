#ifndef OH_H
#define UI_H

#include <iostream>
#include <string>

#include "frc/WPILib.h"
#include "frc/Joystick.h"

using namespace frc;

class OI {
    public:
        OI(){
			std::cout << "OI initiated" << std::endl;	
        }
        Joystick * stick;
        Joystick createStick(int port);
};

#endif
